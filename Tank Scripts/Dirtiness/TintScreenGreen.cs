﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TintScreenGreen : MonoBehaviour {
	DataManager data;

	public GameObject tint;
	Image tintImage;
	byte MAX_ALPHA;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();

		tintImage = tint.GetComponent<Image>();
		MAX_ALPHA = 155;
	}

	void Update () {
		float newAlpha = (data.dirtiness/49.0f) * MAX_ALPHA;
		tintImage.color = new Color32(18, 54, 15, (byte)newAlpha);
	}
}
