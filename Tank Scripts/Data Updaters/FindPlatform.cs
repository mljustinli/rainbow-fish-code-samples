﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FindPlatform : MonoBehaviour {

	public string platform = "";

	// Use this for initialization
	void Awake () {
		if (SystemInfo.deviceType == DeviceType.Desktop) {
			platform = "desktop";
		} else if (SystemInfo.deviceType == DeviceType.Handheld) {
			platform = "handheld";
		}
	}
}
