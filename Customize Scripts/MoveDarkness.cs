﻿using UnityEngine;
using System.Collections;

public class MoveDarkness : MonoBehaviour {
	DataManager data;

	public GameObject male;
	public GameObject female;
	public GameObject other;
	public GameObject darkness;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
	}

	// Update is called once per frame
	void Update () {
		if (data.gender.Equals("male")) {
			darkness.transform.position = male.transform.position;
		} else if (data.gender.Equals("female")) {
			darkness.transform.position = female.transform.position;
		} else {
			darkness.transform.position = other.transform.position;
		}
	}
}
