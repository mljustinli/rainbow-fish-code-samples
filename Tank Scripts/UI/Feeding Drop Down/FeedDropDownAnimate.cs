﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FeedDropDownAnimate : MonoBehaviour {

	public GameObject Feed;
	public GameObject Pebbles;
	public GameObject Normal;
	public GameObject Super;
	public GameObject Vet;
	public GameObject DownArrow;
	GameObject[] expandables;

	Vector3 PebblesV;
	Vector3 NormalV;
	Vector3 SuperV;
	Vector3 VetV;
	Vector3 DownArrowV;
	Vector3[] vectors;

	public bool up;
	float fraction;

	void Start () {
		expandables = new GameObject[5] {Pebbles, Normal, Super, Vet, DownArrow};
		PebblesV = new Vector3();
		NormalV = new Vector3();
		SuperV = new Vector3();
		VetV = new Vector3();
		DownArrowV = new Vector3();
		vectors = new Vector3[5] {PebblesV, NormalV, SuperV, VetV, DownArrowV};
		for (int i = 0; i < expandables.Length; i++) {
			vectors[i] = expandables[i].transform.position;
			expandables[i].transform.position = Feed.transform.position;
		}
		up = false;
		fraction = 0.5f;
	}

	void Update () {
		if (!up) {
			for (int i = 0; i < expandables.Length; i++) {
				expandables[i].transform.position = Vector3.Lerp (expandables[i].transform.position, Feed.transform.position, fraction);
				expandables[i].transform.localScale = Vector3.Lerp (expandables[i].transform.localScale, new Vector3(0.5f, 0.5f, 0.5f), fraction);
			}
		} else if (up) {
			for (int i = 0; i < expandables.Length; i++) {
				expandables[i].transform.position = Vector3.Lerp (expandables[i].transform.position, vectors[i], fraction);
				expandables[i].transform.localScale = Vector3.Lerp (expandables[i].transform.localScale, new Vector3(1.0f, 1.0f, 1.0f), fraction);
			}
		}
	}

	public void FeedDropChange() {
		up = !up;
	}
}
