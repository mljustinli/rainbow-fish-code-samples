﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AnimateGettingMoney : MonoBehaviour {

	public GameObject plusMoney;
	public Camera cam;
	public Canvas cvs;

	List<GameObject> plusUse;
	List<GameObject> plusStore;
	float upSpeed;

	void Start () {
		plusUse = new List<GameObject>();
		plusStore = new List<GameObject>();
		for (int i = 0; i < 52; i++) {
			GameObject newGameObject = Instantiate (plusMoney, new Vector3(-10, -10, 0), Quaternion.identity) as GameObject;
			newGameObject.transform.SetParent(cvs.transform);
			newGameObject.transform.localScale = new Vector3(1, 1, 1);
			plusStore.Add(newGameObject);
		}
		upSpeed = 0.05f;
	}

	void Update () {
		for (int i = 0; i < plusUse.Count; i++) {
			GameObject obj = plusUse[i];
			if (cam.WorldToScreenPoint(plusUse[i].transform.position).y > Screen.height/6) {
				obj.transform.position = new Vector3(-10, -10, 0);
				obj.GetComponent<Text>().color = new Color32(255, 255, 255, 255);
				plusUse.Remove (obj);
				plusStore.Add(obj);
				i--;
			} else {
				Vector3 tempPos = obj.transform.position;
				obj.transform.position = new Vector3(tempPos.x, tempPos.y + upSpeed, 0);
			}
		}
	}

	public void animatePlus(Vector3 coord) {
		if (plusStore.Count > 0) {
			GameObject obj = plusStore[0];
			obj.transform.position = coord;
			plusStore.RemoveAt(0);
			plusUse.Add(obj);
		}
	}
}
