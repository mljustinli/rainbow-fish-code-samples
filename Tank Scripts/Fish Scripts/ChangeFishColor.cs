﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeFishColor : MonoBehaviour {

	DataManager data;
	Animator anim;

	public GameObject healthHandle;
	Sprite[] sprites;
	Image hHandle;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
		anim = GetComponent<Animator>();
		anim.SetInteger("FishColorNumber", data.color);

		sprites = Resources.LoadAll<Sprite>("Sprites/Opaque Fish/FishModel");
		Sprite newt = sprites[data.color * 3 + 1];
		hHandle = healthHandle.GetComponent<Image>();
		hHandle.sprite = newt;
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.I)) {
			data.color += 1;
			data.color %= sprites.Length;
		} else if (Input.GetKeyDown(KeyCode.K)) {
			data.color -= 1;
			data.color += sprites.Length;
			data.color %= sprites.Length;
		}
		anim.SetInteger("FishColorNumber", data.color);
		Sprite newt = sprites[data.color * 3 + 1];
		hHandle.sprite = newt;
	}
}
