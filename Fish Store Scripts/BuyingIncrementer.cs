﻿using UnityEngine;
using System.Collections;

public class BuyingIncrementer : MonoBehaviour {
	DataManager data;

	int pInc;
	int nInc;
	int sInc;
	int vInc;

	float pVal;
	float nVal;
	float sVal;
	float vVal;

	int max;

	void Start() {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();

		pInc = 20;
		nInc = 10;
		sInc = 5;
		vInc = 1;

		pVal = 0.5f;
		nVal = 2.5f;
		sVal = 5.0f;
		vVal = 10.0f;

		max = 160;
	}

	public void addP() {
		if (data.money >= pVal && data.food[0] <= max - pInc) {
			data.food[0] += pInc;
			data.money -= pVal;
		}
	}
	public void addN() {
		if (data.money >= nVal && data.food[1] <= max - nInc) {
			data.food[1] += nInc;
			data.money -= nVal;
		}
	}
	public void addS() {
		if (data.money >= sVal && data.food[2] <= max - sInc) {
			data.food[2] += sInc;
			data.money -= sVal;
		}
	}
	public void addV() {
		if (data.money >= vVal && data.food[3] <= max - vInc) {
			data.food[3] += vInc;
			data.money -= vVal;
		}
	}
}
