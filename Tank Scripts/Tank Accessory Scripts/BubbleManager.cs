﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BubbleManager : MonoBehaviour {

	public GameObject bubble1;
	public Camera cam;

	int counter;
	int MAX_COUNTER;
	List<GameObject> bubbleList;

	// Use this for initialization
	void Start () {
		counter = 0;
		MAX_COUNTER = 5;
		bubbleList = new List<GameObject>();
	}

	// Update is called once per frame
	void Update () {
		counter++;
		if (counter > MAX_COUNTER) {
			counter %= MAX_COUNTER;

			if (Random.Range(0, 10) > 3) {
				//position
				int offset = Random.Range(-10, 10);
				Vector3 tempPos;
				if (Random.Range(0, 100) > 5) {
					tempPos = new Vector3((Screen.width/3) * 2 + offset, -20, 0);
				} else {
					tempPos = new Vector3(Random.Range(0, Screen.width) + offset, -20, 0);
				}
				tempPos = cam.ScreenToWorldPoint(tempPos);
				print (tempPos);
				//object instantiation
				GameObject newGameObject = Instantiate (bubble1, new Vector3(tempPos.x, tempPos.y, 0),
					Quaternion.Euler(0, 0, Random.Range(0, 360))) as GameObject;
				//sorting layer
				if (Random.Range (0, 2) == 0) {
					newGameObject.GetComponent<SpriteRenderer>().sortingOrder = 0;
				} else {
					newGameObject.GetComponent<SpriteRenderer>().sortingOrder = 3;
				}
				//scale
				float newScale = Random.Range(5, 30)/100.0f;
				newGameObject.transform.localScale = new Vector3(newScale, newScale, 0);
				bubbleList.Add(newGameObject);
			}
		}

		moveBubbles();
	}

	void moveBubbles() {
		for (int i = 0; i < bubbleList.Count; i++) {
			Vector3 temp = bubbleList[i].transform.position;
			if (cam.WorldToScreenPoint(temp).y > Screen.height + 20) {
				Destroy (bubbleList[i]);
				bubbleList.RemoveAt(i);
				i--;
			} else {
				float upSpeed = Random.Range(5, 7)/100.0f;
				bubbleList[i].transform.position = new Vector3(temp.x, temp.y + upSpeed, temp.z);
				bubbleList[i].transform.Rotate(0, 0, Random.Range(0, 3));
			}
		}
	}
}
