﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeCustomizeFishColor : MonoBehaviour {
	DataManager data;

	Sprite[] sprites;

	public Image fishImage;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();

		sprites = Resources.LoadAll<Sprite>("Sprites/Opaque Fish/FishModel");
	}

	// Update is called once per frame
	void Update () {
		fishImage.sprite = sprites[data.color * 3 + 1];
	}

	public void addOneColor() {
		data.color++;
		data.color %= sprites.Length/3;
	}
}
