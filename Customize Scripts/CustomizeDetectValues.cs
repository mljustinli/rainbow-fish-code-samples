﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CustomizeDetectValues : MonoBehaviour {
	DataManager data;

	public GameObject NameInputField;
	public GameObject GenderDropDown;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
		data.Reset();
		data.resetOnLeave = true;
	}

	public void WriteNewInfo() {
		string[] genders = new string[] {"male", "female", "other"};
		int holdColor = data.color;
		string holdGender = data.gender;

		data.exists = true;
		data.resetOnLeave = false;
		data.Reset();

		data.color = holdColor;
		data.gender = holdGender;

		data.name = NameInputField.GetComponent<InputField>().text;

		Application.LoadLevel("Tank");
	}

	public void setMale() {
		data.gender = "male";
	}

	public void setFemale() {
		data.gender = "female";
	}

	public void setOther() {
		data.gender = "other";
	}
}
