﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FeedingManager : MonoBehaviour {
	DataManager data;
	FindPlatform find;

	//Pf means Pebble food sprite
	public GameObject Pf;
	public GameObject Nf;
	public GameObject Sf;
	public GameObject Vf;
	public GameObject[] placeables;

	public bool up;
	public bool place;
	public int toBePlaced;

	public Camera cam;
	public List<GameObject> foodList;
	float downSpeed;

	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
		find = GameObject.FindGameObjectWithTag("Platform").GetComponent<FindPlatform>();

		placeables = new GameObject[4] {Pf, Nf, Sf, Vf};

		up = true;
		place = false;
		toBePlaced = 0;

		foodList = new List<GameObject>();
		downSpeed = 0.05f;
	}

	void Update () {
		if (data.food[toBePlaced] == 0) {
			place = false;
		}

		if (Input.GetMouseButtonDown(0) && place) {
			if ((Input.touches.Length > 0 && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId) && find.platform.Equals("handheld"))
			    || (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && find.platform.Equals("desktop"))) {
				Vector3 tempPos = cam.ScreenToWorldPoint(Input.mousePosition);
				GameObject newGameObject = Instantiate (placeables[toBePlaced], new Vector3(tempPos.x, tempPos.y, 0), Quaternion.Euler(0, 0, Random.Range(0, 360))) as GameObject;
				foodList.Add(newGameObject);
				data.food[toBePlaced] -= 1;
			}
		}

		if (foodList.Count > 0) {
			for (int i = 0; i < foodList.Count; i++) {
				Vector3 temp = foodList[i].transform.position;
				if (cam.WorldToScreenPoint(temp).y < 0) {
					Destroy (foodList[i]);
					//need to remove it or else game crashes!
					foodList.RemoveAt(i);
					i--;
					//tank gets dirtier!
					data.dirtiness = Mathf.Clamp(data.dirtiness + 1, 0, 49);
				} else {
					foodList[i].transform.position = new Vector3(temp.x, temp.y - downSpeed, temp.z);
					foodList[i].transform.Rotate(0, 0, Random.Range(0, 3));
				}
			}
		}
	}

	public void Pebbles() {
		if (place && toBePlaced == 0) {
			place = false;
		} else {
			place = true;
		}
		toBePlaced = 0;
	}
	public void Normal() {
		if (place && toBePlaced == 1) {
			place = false;
		} else {
			place = true;
		}
		toBePlaced = 1;

	}
	public void Super() {
		if (place && toBePlaced == 2) {
			place = false;
		} else {
			place = true;
		}
		toBePlaced = 2;

	}
	public void Vet() {
		if (place && toBePlaced == 3) {
			place = false;
		} else {
			place = true;
		}
		toBePlaced = 3;
	}

	public void changeUp() {
		if(!up) {
			place = false;
		}
		up = !up;
	}
}
