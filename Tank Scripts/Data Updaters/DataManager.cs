﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

//singletonish behavior
public class DataManager : MonoBehaviour {

	public static DataManager control;

	//data variables
	public int version;
	public String name;
	public String gender;
	public int color;
	public int health;
	public int hpast;
	public int satiety;
	public int spast;
	public int dirtiness;
	public int dpast;
	public Buildup[] buildupList;

	public float money;

	public int[] food;

	String saveTo = "/fishData.dat";

	public bool resetOnLeave = false;
	System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

	public bool exists = false;

	void Awake() {
		if (control == null) {
			DontDestroyOnLoad(gameObject);
			control = this;
		} else if (control != this) {
			Destroy (gameObject);
		}

		Reset ();
		Load ();
	}

	void OnApplicationFocus(bool focusStatus) {
		if (focusStatus == false) {
			if (resetOnLeave) {
				File.Delete(Application.persistentDataPath + saveTo);
			} else {
				print ("saved");
				Save ();
			}
		}
	}

	//also default values
	public void Reset() {
		version = 0;
		name = "Ares";
		gender = "male";
		color = 0;
		health = 100;
		hpast = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		satiety = 100;
		spast = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		dirtiness = 0;
		dpast = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		buildupList = new Buildup[7];

		money = 20.0f;

		food = new int[4] {50, 20, 5, 1};
	}

	public void Save() {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + saveTo);

		PlayerData data = new PlayerData();

		data.version = version;
		data.name = name;
		data.gender = gender;
		data.color = color;
		data.health = health;
		data.hpast = hpast;
		data.satiety = satiety;
		data.spast = spast;
		data.dirtiness = dirtiness;
		data.dpast = dpast;
		data.buildupList = buildupList;

		data.money = money;

		data.thing = food;

		bf.Serialize(file, data);
		file.Close ();
	}

	public void Load() {
		if (File.Exists (Application.persistentDataPath + saveTo)) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open (Application.persistentDataPath + saveTo, FileMode.Open);
			PlayerData data = (PlayerData) bf.Deserialize(file);
			file.Close();

			//here
			version = data.version;
			name = data.name;
			gender = data.gender;
			color = data.color;
			health = data.health;
			hpast = data.hpast;
			satiety = data.satiety;
			spast = data.spast;
			dirtiness = data.dirtiness;
			dpast = data.dpast;
			buildupList = data.buildupList;

			money = data.money;

			food = data.thing;

			exists = true;
		} else {
			exists = false;
		}
	}
}

[Serializable]
class PlayerData {
	public int version;
	public String name;
	public String gender;
	public int color;
	public int health;
	public int hpast;
	public int satiety;
	public int spast;
	public int dirtiness;
	public int dpast;
	public Buildup[] buildupList;

	public float money;

	public int[] thing;
}
