﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateMoney : MonoBehaviour {
	DataManager data;

	public Text moneyText;

	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
	}

	void Update () {
		moneyText.text = "Money: $" + data.money.ToString("F2");
	}
}
