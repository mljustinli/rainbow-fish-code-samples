﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeGender : MonoBehaviour {
	DataManager data;

	public Image image;
	public Sprite other;
	public Sprite male;
	public Sprite female;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();

		if (data.gender.Equals("male")) {
			image.sprite = male;
		} else if (data.gender.Equals("female")) {
			image.sprite = female;
		} else {
			image.sprite = other;
		}
	}
}
