﻿using UnityEngine;
using System.Collections;

public class FishMovement : MonoBehaviour {

	public Camera cam;
	Animator anim;

	float screenWidth;
	float screenHeight;

	bool regSwimming;
	Vector2 screenCoords;
	Vector2 movement;
	float angle;
	float magnitude;
	float maxMag;
	float moveTime;
	int moveTimeRange;

	float scale;
	int modScale;

	float past;

	System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
	int cur_time;

	float feedSwimFraction;
	FeedingManager feed;
	DataManager data;
	Hashtable foodIncrease = new Hashtable();

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();

		screenWidth = Screen.width;
		screenHeight = Screen.height;

		regSwimming = true;
		screenCoords = new Vector2(screenWidth/2, screenHeight/2);
		movement = new Vector2(0, 0);
		magnitude = 10;
		moveTime = 2.0f;
		moveTimeRange = 3;

		scale = 0.5f;
		modScale = 1;

		past = Time.time;

		feedSwimFraction = 4.0f;
		feed = GameObject.FindGameObjectWithTag("Feed").GetComponent<FeedingManager>();
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
		foodIncrease.Add("Pebble", 1);
		foodIncrease.Add ("Normal", 5);
		foodIncrease.Add ("Super", 20);
		foodIncrease.Add ("Vet", 50);

		anim.SetBool("Swimming", true);
	}

	void Update() {
		cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
	}

	void FixedUpdate () {
		CheckMoves();
	}

	void CheckMoves() {
		SwimToFood();
		RegMove ();
	}

	void RegMove() {
		if (regSwimming) {
			if (Time.time - past >= moveTime) {
				moveTime = Random.Range(1, moveTimeRange);
				angle = Random.Range(0, 2 * Mathf.PI);
				magnitude = 20;
				maxMag = 10;
				movement = magnitude * new Vector2(Mathf.Cos(angle), Mathf.Sin (angle));
				Rotate ();
				past = Time.time;
			}
			magnitude *= 0.9f;
			anim.speed = magnitude/2;
			movement = magnitude * new Vector2(Mathf.Cos(angle), Mathf.Sin (angle));

			screenCoords += movement;
			screenCoords.x += screenWidth;
			screenCoords.x %= screenWidth;
			screenCoords.y += screenHeight;
			screenCoords.y %= screenHeight;

			Vector3 temp = cam.ScreenToWorldPoint(screenCoords);
			transform.position = new Vector3(temp.x, temp.y, 0);
		}
	}

	void Rotate() {
		float dAngle = (angle * 180)/Mathf.PI;
		if (dAngle <= 270 && dAngle >= 90) {
			modScale = -1;
		} else {
			modScale = 1;
		}
		transform.localScale = new Vector3(scale, modScale * scale, 1);
		transform.rotation = Quaternion.Euler(0, 0, (angle * 180)/Mathf.PI);
	}

	void SwimToFood() {
		if (feed.foodList.Count > 0 && data.satiety < 100) {
			regSwimming = false;
			float dx = feed.foodList[0].transform.position.x - transform.position.x;
			float dy = feed.foodList[0].transform.position.y - transform.position.y;
			angle = (Mathf.Atan2(dy, dx) + 2 * Mathf.PI) % (2 * Mathf.PI);
			Rotate ();

			transform.position = Vector3.Lerp(transform.position, feed.foodList[0].transform.position, feedSwimFraction * Time.deltaTime);
			screenCoords = cam.WorldToScreenPoint(transform.position);

			anim.speed = (100 - data.satiety)/3.0f;
		} else {
			regSwimming = true;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (data.satiety < 100 && (other.tag == "Pebble" || other.tag == "Normal" || other.tag == "Super" || other.tag == "Vet")) {
			data.satiety = Mathf.Clamp(data.satiety + (int)foodIncrease[other.tag], 0, 100);
			feed.foodList.Remove (other.gameObject);
			Destroy(other.gameObject);
		}
	}
}
