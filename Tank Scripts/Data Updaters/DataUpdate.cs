﻿using UnityEngine;
using System.Collections;

public class DataUpdate : MonoBehaviour {
	DataManager data;
	System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

	int sinterval = 1728;
	int dinterval = 1764;
	int hinterval = 2;

	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();

		if (data.hpast == 0) {
			data.hpast = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		}

		if (data.spast == 0) {
			data.spast = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		}
	}

	void Update() {
		Satiety();
		Dirtiness();
		Health();
	}

	void Health() {
		int cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		int diff = cur_time - data.hpast;
		if ((data.satiety <= 20 || data.dirtiness >= 30)&& diff >= hinterval) {
			data.health = Mathf.Clamp(data.health - diff/hinterval, 0, 100);
			data.hpast = cur_time - (diff % hinterval);
		} else if (data.satiety > 20 && diff >= hinterval) {
			data.health = Mathf.Clamp (data.health + diff/hinterval, 0, 100);
			data.hpast = cur_time - (diff % hinterval);
		}
	}

	void Satiety() {
		int cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		int diff = cur_time - data.spast;
		if (diff >= sinterval) {

			int currentSatiety = Mathf.Clamp(data.satiety - diff/sinterval, 0, 100);
			if (data.satiety > 20 && currentSatiety < 20) {
				int dtime = sinterval * (data.satiety - 20);
				int timeTwentyReached = data.spast + dtime;
				data.hpast = timeTwentyReached;
				data.health = Mathf.Clamp (data.health + 1 * dtime/hinterval, 0, 100);
			}

			if (data.satiety == 21) {
				data.hpast = cur_time;
			}

			data.satiety = Mathf.Clamp(data.satiety - diff/sinterval, 0, 100);
			data.spast = cur_time - (diff % sinterval);
		}
	}

	void Dirtiness() {
		int cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		int diff = cur_time - data.dpast;
		if (diff >= dinterval) {
			int currentDirtiness = Mathf.Clamp (data.dirtiness + diff/dinterval, 0, 49);
			if (data.dirtiness < 30 && currentDirtiness > 30) {
				int dtime = dinterval * (currentDirtiness - 30);
				int timeThirtyReached = data.dpast + dtime;
				data.hpast = timeThirtyReached;
			}

			if (data.dirtiness == 29) {
				data.hpast = cur_time;
			}

			data.dirtiness = Mathf.Clamp(data.dirtiness + diff/dinterval, 0, 49);
			data.dpast = cur_time - (diff % dinterval);
		}
	}
}
