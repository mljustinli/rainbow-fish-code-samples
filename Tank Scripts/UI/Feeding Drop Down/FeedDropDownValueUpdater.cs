﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FeedDropDownValueUpdater : MonoBehaviour {
	DataManager data;
	FeedingManager feed;

	int size;
	int namePos;
	int amountPos;
	int increasePos;
	byte alpha;

	public GameObject FeedingExpand;
	GameObject[] dropDowns;
	Text[] nameTexts;
	Text[] amountTexts;
	Text[] increaseTexts;

	int[] increases = new int[4] {1, 5, 20, 50};

	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
		feed = GameObject.FindGameObjectWithTag("Feed").GetComponent<FeedingManager>();

		size = 4;
		namePos = 0;
		amountPos = 1;
		increasePos = 2;
		alpha = 255;

		dropDowns = new GameObject[size];
		nameTexts = new Text[size];
		amountTexts = new Text[size];
		increaseTexts = new Text[size];

		for (int i = 0; i < FeedingExpand.transform.childCount; i++) {
			dropDowns[i] = FeedingExpand.transform.GetChild(i).gameObject;
		}
		for (int i = 0; i < size; i++) {
			nameTexts[i] = dropDowns[i].transform.GetChild(namePos).gameObject.GetComponent<Text>();
			amountTexts[i] = dropDowns[i].transform.GetChild(amountPos).gameObject.GetComponent<Text>();
			increaseTexts[i] = dropDowns[i].transform.GetChild(increasePos).gameObject.GetComponent<Text>();
			increaseTexts[i].text = "+" + increases[i];
		}
	}

	void Update () {
		for (int i = 0; i < size; i++) {
			amountTexts[i].text = "x" + data.food[i];
		}
		changeCurrentButton();
	}

	void changeCurrentButton() {
		for (int i = 0; i < size; i++) {
			if (feed.place && i == feed.toBePlaced) {
				Button b = dropDowns[i].GetComponent<Button>();
				ColorBlock cb = b.colors;
				cb.normalColor = new Color32(99, 10, 10, alpha);
				b.colors = cb;
			} else {
				Button b = dropDowns[i].GetComponent<Button>();
				ColorBlock cb = b.colors;
				cb.normalColor = new Color32(142, 8, 8, alpha);
				b.colors = cb;
			}
		}
	}

	void change(int pos) {
		nameTexts[pos].fontStyle = FontStyle.Bold;
		amountTexts[pos].fontStyle = FontStyle.Bold;
		increaseTexts[pos].fontStyle = FontStyle.Bold;
	}
}
