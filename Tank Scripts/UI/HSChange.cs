﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HSChange : MonoBehaviour {
	DataManager data;

	public GameObject healthBar;
	public GameObject satietyBar;
	Slider healthSlider;
	Slider satietySlider;

	public GameObject healthFill;
	public GameObject satietyFill;
	Image healthImage;
	Image satietyImage;

	public Text healthText;
	public Text satietyText;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
		healthSlider = healthBar.GetComponent<Slider>();
		satietySlider = satietyBar.GetComponent<Slider>();

		healthImage = healthFill.GetComponent<Image>();
		satietyImage = satietyFill.GetComponent<Image>();
	}

	// Update is called once per frame
	void Update () {
		healthSlider.value = data.health;
		satietySlider.value = data.satiety;
		updateHealthColor ();
		updateSatietyColor ();
		updateHealthText();
		updateSatietyText();
	}

	void updateHealthColor() {
		if (data.health > 40) {
			healthImage.color = new Color32(36, 223, 84, 255);
		} else if (data.health > 10) {
			healthImage.color = new Color32(234, 219, 17, 255);
		} else {
			healthImage.color = new Color32(193, 14, 14, 255);
		}
	}

	void updateSatietyColor() {
		if (data.satiety > 40) {
			satietyImage.color = new Color32(36, 223, 84, 255);
		} else if (data.satiety > 10) {
			satietyImage.color = new Color32(234, 219, 17, 255);
		} else {
			satietyImage.color = new Color32(193, 14, 14, 255);
		}
	}

	void updateHealthText() {
		healthText.text = "Health:  " + data.health + "%";
	}

	void updateSatietyText() {
		satietyText.text = "Satiety: " + data.satiety + "%";
	}
}
