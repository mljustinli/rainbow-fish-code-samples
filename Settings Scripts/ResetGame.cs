﻿using UnityEngine;
using System.Collections;

public class ResetGame : MonoBehaviour {
	DataManager data;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
	}

	public void Reset() {
		data.resetOnLeave = true;
		data.exists = false;
	}
}
