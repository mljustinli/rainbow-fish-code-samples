﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DirtinessCollectionManager : MonoBehaviour {
	DataManager data;
	public Camera cam;
	public GameObject buildup;
	public AnimateGettingMoney animateGettingMoney;

	int numOfPics;
	int buildupSum;

	Sprite[] buildupSprites;
	GameObject[] buildupObjects;

	float moneyGainedFromCleaning;

	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();

		numOfPics = 7;
		buildupSum = 0;

		buildupSprites = Resources.LoadAll<Sprite>("Sprites/Dirtiness/algaeCollect");
		buildupObjects = new GameObject[numOfPics];

		moneyGainedFromCleaning = 0.03f;

		if (data.buildupList[0] == null) {
			data.buildupList = new Buildup[numOfPics];
			newBuildupList();
		}

		newGameObjectList();
	}

	void Update () {


		if (Input.GetKeyDown(KeyCode.P)) {
			newBuildupList();
		}
		if (Input.GetKeyDown(KeyCode.RightBracket)) {
			add ();
			data.dirtiness++;
		}

		buildupSum = 0;
		for (int i = 0; i < data.buildupList.Length; i++) {
			buildupSum += data.buildupList[i].value;
		}

		if (buildupSum < data.dirtiness) {
			for (int i = 0; i < data.dirtiness - buildupSum; i++) {
				add ();
			}
		}
	}

	void add() {
		int index = Random.Range(0, numOfPics);
		int orig = index;

		while (data.buildupList[index].value == numOfPics) {
			index = (index + 1) % numOfPics;
			if (index == orig) {
				return;
			}
		}

		data.buildupList[index].value++;
		buildupObjects[index] = changeSprite(data.buildupList[index].value - 1, buildupObjects[index]);
	}

	public void subtract(GameObject obj) {
		string tag = obj.tag;
		int index;

		int.TryParse(tag[tag.Length - 1] + "", out index);
		data.buildupList[index].value--;
		data.dirtiness--;
		//add money
		data.money += moneyGainedFromCleaning;
		animateGettingMoney.animatePlus(obj.transform.position);
		changeSprite (data.buildupList[index].value - 1, buildupObjects[index]);
	}

	void newBuildupList() {
		for (int i = 0; i < data.buildupList.Length; i++) {
			data.buildupList[i] = newBuildupObject();
		}
	}

	Buildup newBuildupObject() {
		Buildup newBuildup = new Buildup(0, Random.Range(0, Screen.width), 0);
		return newBuildup;
	}

	void newGameObjectList() {
		for (int i = 0; i < buildupObjects.Length; i++) {
			GameObject newGameObject = Instantiate (buildup, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			Vector3 temp = cam.ScreenToWorldPoint(new Vector3(data.buildupList[i].x, data.buildupList[i].y, 0));
			newGameObject.transform.position = new Vector3(temp.x, temp.y + newGameObject.GetComponent<BoxCollider2D>().bounds.size.y/2, 0);

			newGameObject = changeSprite(data.buildupList[i].value - 1, newGameObject);
			newGameObject.tag = "buildup" + i;

			buildupObjects[i] = newGameObject;
		}
	}

	GameObject changeSprite(int value, GameObject obj) {
		if (value == -1) {
			obj.SetActive(false);
		} else {
			obj.GetComponent<SpriteRenderer>().sprite = buildupSprites[value];
			obj.SetActive(true);
		}
		return obj;
	}
}

[System.Serializable]
public class Buildup {
	public int value;
	public int x;
	public int y;

	public Buildup(int _value, int _x, int _y) {
		value = _value;
		x = _x;
		y = _y;
	}
}
