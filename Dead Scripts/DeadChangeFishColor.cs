﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeadChangeFishColor : MonoBehaviour {
	DataManager data;

	public Image fish;
	Sprite[] sprites;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
		sprites = Resources.LoadAll<Sprite>("Sprites/Opaque Fish/FishModel");
	}

	// Update is called once per frame
	void Update () {
		fish.sprite = sprites[data.color * 3 + 1];
	}
}
