﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowCurrentAmount : MonoBehaviour {
	DataManager data;

	public Text pAmount;
	public Text nAmount;
	public Text sAmount;
	public Text vAmount;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
	}

	// Update is called once per frame
	void Update () {
		pAmount.text = "1" + "\n" + "$0.50" + "\n" + "\n" + data.food[0];
		nAmount.text = "5" + "\n" + "$2.50" + "\n" + "\n" + data.food[1];
		sAmount.text = "20" + "\n" + "$5.00" + "\n" + "\n" + data.food[2];
		vAmount.text = "50" + "\n" + "$10.00" + "\n" + "\n" + data.food[3];
	}
}
