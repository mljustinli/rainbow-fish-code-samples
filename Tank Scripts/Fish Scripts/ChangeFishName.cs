﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeFishName : MonoBehaviour {
	DataManager data;

	public Text nameText;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
		nameText.text = data.name;
	}
}
