﻿using UnityEngine;
using System.Collections;

public class OnDeath : MonoBehaviour {
	DataManager data;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
	}

	// Update is called once per frame
	void Update () {
		if (data.health == 0) {
			Application.LoadLevel("Dead");
		}
	}
}
