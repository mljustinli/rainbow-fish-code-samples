﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyChange : MonoBehaviour {
	DataManager data;

	public Text moneyText;

	// Use this for initialization
	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<DataManager>();
	}

	// Update is called once per frame
	void Update () {
		moneyText.text = "Money: $" + data.money.ToString("F2");
	}
}
