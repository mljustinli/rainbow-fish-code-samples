﻿using UnityEngine;
using System.Collections;

public class CheckClickOnBuildup : MonoBehaviour {
	DirtinessCollectionManager buildupManager;
	FeedingManager feed;

	// Use this for initialization
	void Start () {
		buildupManager = GameObject.FindGameObjectWithTag("BuildupManager").GetComponent<DirtinessCollectionManager>();
		feed = GameObject.FindGameObjectWithTag("Feed").GetComponent<FeedingManager>();
	}

	void OnMouseOver() {
		if (Input.GetMouseButtonDown(0) && feed.place == false) {
			buildupManager.subtract(this.gameObject);
		}
	}
}
